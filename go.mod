module oqqer.com/chatbot

go 1.13

require (
	github.com/VictoriaMetrics/fastcache v1.5.1
	github.com/coreos/pkg v0.0.0-20180928190104-399ea9e2e55f
	github.com/davecgh/go-spew v1.1.1
	github.com/dghubble/go-twitter v0.0.0-20190719072343-39e5462e111f
	github.com/dghubble/oauth1 v0.6.0
	github.com/goburrow/cache v0.1.1-0.20180410174946-576a8d1542c8
	github.com/nlopes/slack v0.6.1-0.20191106133607-d06c2a2b3249
	github.com/pkg/errors v0.8.1
	github.com/prometheus/client_golang v1.1.0
	github.com/rs/zerolog v1.15.0
	github.com/yosssi/gmq v0.0.1
)
