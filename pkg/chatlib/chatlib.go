package chatlib

import (
	"fmt"
	"github.com/yosssi/gmq/mqtt"
	"github.com/yosssi/gmq/mqtt/client"
)

type Message struct {
	Id      uint64 `json:"id"`
	Type    string `json:"type"`
	Channel string `json:"channel"`
	Text    string `json:"text"`
	User    string `json:"user"`
}

type PostMessage struct {
	Channel     string       `json:"channel"`
	Text        string       `json:"text"`
	Attachments []Attachment `json:"attachments"`
}

type TopicMessage struct {
	Channel string `json:"channel"`
	Topic   string `json:"topic"`
}

type Attachment struct {
	Fallback   string   `json:"fallback"`
	AuthorName string   `json:"author_name"`
	AuthorLink string   `json:"author_link"`
	AuthorIcon string   `json:"author_icon"`
	Title      string   `json:"title"`
	TitleLink  string   `json:"title_link"`
	Text       string   `json:"text"`
	Footer     string   `json:"footer"`
	FooterIcon string   `json:"footer_icon"`
	Color      string   `json:"color"`
	MarkdownIn []string `json:"mrkdwn_in"`
	Image      string   `json:"image_url"`
}

type StopFollowingThreadAction struct {
	StatusId string `json:"status_id"`
	Channel  string `json:"channel"`
}

func Connect(clientName string) (cli *client.Client, err error) {
	// Create an MQTT Client.
	cli = client.New(&client.Options{
		ErrorHandler: func(err error) {
			fmt.Println(err)
		},
	})

	// Terminate the Client.
	defer cli.Terminate()

	// Connect to the MQTT Server.
	err = cli.Connect(&client.ConnectOptions{
		Network:  "tcp",
		Address:  "mqtt:1883",
		ClientID: []byte(clientName),
	})
	if err != nil {
		return
	}

	return
}

func Subscribe(cli *client.Client, topic string, handler client.MessageHandler) (err error) {
	// Subscribe to topics.
	err = cli.Subscribe(&client.SubscribeOptions{
		SubReqs: []*client.SubReq{
			{
				// TopicFilter is the Topic Filter of the Subscription.
				TopicFilter: []byte(topic),
				// QoS is the requsting QoS.
				QoS: mqtt.QoS2,
				// Handler is the handler which handles the Application Message
				// sent from the Server.
				Handler: handler,
			},
		},
	})
	return
}
