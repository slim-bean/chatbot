package twitter

import (
	"encoding/json"
	"io/ioutil"
)

/*
[
  {
    "screenName": "",
    "id": "",
	"color": "",
    "channels": [
      {
        "channelId": "",
        "displayFromUser": true,
        "displayRetweetsOfOtherUsersTweets": true,
        "displayOtherUserRetweetOfUs": false,
        "displayMessagesToOtherUsers": false,
        "displayMessagesFromOtherUsers": false
      }
    ]
  }
]
*/

type twitterUser struct {
	ScreenName string           `json:"screenName"`
	Id         string           `json:"id"`
	Color      string           `json:"color"`
	Channels   []channelOptions `json:"channels"`
}

type channelOptions struct {
	ChannelId                        string `json:"channelId"`
	DisplayFromUser                  bool   `json:"displayFromUser"`
	DisplayRetweetsOfOtherUserTweets bool   `json:"displayRetweetsOfOtherUsersTweets"`
	DisplayOtherUserRetweetOfUs      bool   `json:"displayOtherUserRetweetOfUs"`
	DisplayMessagesToOtherUsers      bool   `json:"displayMessagesToOtherUsers"`
	DisplayMessagesFromOtherUsers    bool   `json:"displayMessagesFromOtherUsers"`
}

//twatter: CFWET8U4S
//roclive: CF9SGRPFC

func LoadFromFile(fileName string) (map[string]*twitterUser, error) {
	rawJson, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, err
	}
	var users []twitterUser
	err = json.Unmarshal(rawJson, &users)
	if err != nil {
		return nil, err
	}
	usersMap := make(map[string]*twitterUser)

	for idx, user := range users {
		usersMap[user.Id] = &users[idx]
	}

	return usersMap, nil
}
