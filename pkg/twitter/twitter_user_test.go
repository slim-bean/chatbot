package twitter

import (
	"testing"
)

func TestLoadFromFile(t *testing.T) {

	users, err := LoadFromFile("../twitter.json")
	if err != nil {
		t.Error(err)
		return
	}

	if users["16275936"].ScreenName != "rachbarnhart" {
		t.Error("Expected rachbarnhart, got", users["16275936"].ScreenName)
		return
	}

}
