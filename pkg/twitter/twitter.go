package twitter

import (
	"encoding/json"
	"github.com/VictoriaMetrics/fastcache"
	"github.com/davecgh/go-spew/spew"
	"github.com/dghubble/go-twitter/twitter"
	"github.com/goburrow/cache"
	"github.com/rs/zerolog/log"
	"strconv"
)

var (
	tweetFormat = "%v: %v"
)

type User struct {
	ID                               string
	DisplayFromUser                  bool
	DisplayRetweetsOfOtherUserTweets bool
	DisplayOtherUserRetweetOfUs      bool
	DisplayMessagesToOtherUsers      bool
	DisplayMessagesFromOtherUsers    bool
}

func defaultUser(id string) *User {
	return &User{
		id,
		true,
		true,
		false,
		false,
		false,
	}
}

type TweetType int

// From the twitter docs the filter on Users will return the following tweets:
//    Tweets created by the user.
//    Tweets which are retweeted by the user.
//    Replies to any Tweet created by the user.
//    Retweets of any Tweet created by the user.
//    Manual replies, created without pressing a reply button (e.g. “@twitterapi I agree”).
const (
	NOT_DEFINED = iota
	CREATED_BY_USER
	RETWEET_CREATED_BY_USER
	REPLY_TO_TWEET_CREATED_BY_USER
	REPLY_TO_OTHER_USER
	RETWEET_OF_TWEET_CREATED_BY_USER
)

type twitterHandler struct {
	twitterClient *twitter.Client
	tweetCache    cache.LoadingCache
	allCache      *fastcache.Cache
	userMap       map[string]*twitterUser
}

func New(twitterClient *twitter.Client, tweetCache cache.LoadingCache, userMap map[string]*twitterUser) *twitterHandler {
	//allCache := gocache.New(24*time.Hour, 10*time.Minute)
	//allCache := freecache.NewCache(200 * 1024 * 1024)
	allCache := fastcache.New(200 * 1024 * 1024)

	return &twitterHandler{
		twitterClient: twitterClient,
		tweetCache:    tweetCache,
		allCache:      allCache,
		userMap:       userMap,
	}
}

func (th *twitterHandler) ProcessTweet(tweet *twitter.Tweet) (map[string]struct{}, TweetType) {
	ts, err := json.Marshal(tweet)
	if err != nil {
		log.Error().Err(err).Msg("Failed to marshal tweet and put it in cache")
	} else {
		th.allCache.Set([]byte(tweet.IDStr), ts)
	}

	tweetType := TweetType(NOT_DEFINED)
	channelMap := make(map[string]struct{})
	var tweetText string
	if tweet.ExtendedTweet != nil {
		tweetText = tweet.ExtendedTweet.FullText
	} else {
		tweetText = tweet.Text
	}

	//Handle retweets
	if tweet.RetweetedStatus != nil {
		//Check to see if this tweet was made by our user
		if usr, ok := th.userMap[tweet.User.IDStr]; ok {
			tweetType = TweetType(RETWEET_CREATED_BY_USER)
			log.Info().Str("sender", tweet.User.ScreenName).Str("tweetId", tweet.IDStr).Bool("fromSender", true).Msgf(tweetFormat, tweet.User.ScreenName, tweetText)
			//Allow if we let our user display retweets
			for _, channel := range usr.Channels {
				if channel.DisplayRetweetsOfOtherUserTweets {
					channelMap[channel.ChannelId] = struct{}{}
					log.Printf("Showing a retweet FROM our user %s for %s\n", usr.Id, channel.ChannelId)
				} else {
					//log.Printf("Not showing a retweet FROM our user %s for %s\n", usr.Id, channel.ChannelId)
				}
			}
		} else {
			//If this message wasn't sent by our user, the retweet is likely of our user, see if it should be shown
			if usr, ok := th.userMap[tweet.RetweetedStatus.User.IDStr]; ok {
				tweetType = TweetType(RETWEET_OF_TWEET_CREATED_BY_USER)
				//log.Info().Str("sender", tweet.User.ScreenName).Str("tweetId", tweet.IDStr).Str("replyTo", tweet.InReplyToScreenName).Str("replyToTweet", tweet.InReplyToStatusIDStr).Msg(tweetText)
				for _, channel := range usr.Channels {
					if channel.DisplayOtherUserRetweetOfUs {
						channelMap[channel.ChannelId] = struct{}{}
						log.Printf("Showing a retweet OF our user %s for %s\n", usr.Id, channel.ChannelId)
					} else {
						//log.Printf("Not showing a retweet OF our user %s for %s\n", usr.Id, channel.ChannelId)
					}
				}
			} else {
				// log.Printf("Somehow we got a retweet which wasn't by us or from us?? %+v\n", tweet.RetweetedStatus)
				// We get retweets of tweets we already retweeted, these show up as not being from us or a retweet of ours
				// and ends up getting dumped here, I don't see anything in the payload we could use to explicitly identify
				// these tweets, so hopefully there aren't other cases that end up here....
				// This isn't really the correct status, but we need to pick something other than NOT_DEFINED and
				// I didn't think this justified it's own status
				tweetType = TweetType(RETWEET_CREATED_BY_USER)
			}
		}
	} else if tweet.InReplyToUserIDStr != "" {
		// Not a retweet, but is a reply, see if it's a reply BY us
		if usr, ok := th.userMap[tweet.User.IDStr]; ok {
			tweetType = TweetType(REPLY_TO_OTHER_USER)
			twt := th.getOriginalTweet(tweet.IDStr)
			if twt != nil {
				// We only care about replies that originated from a tweet our user sent, we will treat this as a reply by another person
				log.Info().Str("replyTo", twt.User.ScreenName).Str("replyToTweet", twt.IDStr).Bool("fromSender", true).Msgf(tweetFormat, tweet.User.ScreenName, tweetText)
			}
			for _, channel := range usr.Channels {
				if tweet.InReplyToUserIDStr == usr.Id && channel.DisplayFromUser {
					//Special case where we allow all replies if the user is replying to themselves
					channelMap[channel.ChannelId] = struct{}{}
					log.Printf("Showing a user reply to themselves for %s\n", channel.ChannelId)
				} else if channel.DisplayMessagesToOtherUsers {
					//This is a reply from one of our users to someone else
					channelMap[channel.ChannelId] = struct{}{}
					log.Printf("Showing a reply FROM our user %s TO someone else for %s\n", usr.Id, channel.ChannelId)
				} else {
					//log.Printf("Not showing a reply FROM our user %s TO someone else for %s\n", usr.Id, channel.ChannelId)
				}
			}
		} else {
			if usr, ok := th.userMap[tweet.InReplyToUserIDStr]; ok {
				tweetType = TweetType(REPLY_TO_TWEET_CREATED_BY_USER)
				twt := th.getOriginalTweet(tweet.IDStr)
				if twt != nil {
					//Keep the reply reference only to the original tweet in the thread
					log.Info().Str("replyTo", twt.User.ScreenName).Str("replyToTweet", twt.IDStr).Msgf(tweetFormat, tweet.User.ScreenName, tweetText)
				}
				//This is a reply from someone else to one of our users
				for _, channel := range usr.Channels {
					if channel.DisplayMessagesFromOtherUsers {
						channelMap[channel.ChannelId] = struct{}{}
						log.Printf("Showing a reply TO our user %s FROM someone else for %s\n", usr.Id, channel.ChannelId)
					} else {
						//log.Printf("Not showing a reply TO our user %s FROM someone else for %s\n", usr.Id, channel.ChannelId)
					}
				}
			} else {
				if tweet.InReplyToUserID == tweet.User.ID {
					tweetType = TweetType(REPLY_TO_TWEET_CREATED_BY_USER)
					twt := th.getOriginalTweet(tweet.IDStr)
					if twt != nil {
						//Even though this is a reply to a reply we want the labels to reflect the original tweet being replied to
						log.Info().Str("replyTo", twt.User.ScreenName).Str("replyToTweet", twt.IDStr).Msgf(tweetFormat, tweet.User.ScreenName, tweetText)
					}
					//This is a reply from someone else to one of our users where they are actually replying to themselves
					usr := th.getOriginalUser(tweet.ID)
					if usr != nil {
						for _, channel := range usr.Channels {
							if channel.DisplayMessagesFromOtherUsers {
								channelMap[channel.ChannelId] = struct{}{}
								log.Printf("Showing a reply TO  %s FROM someone else (this is actually a reply to a reply) for %s\n", usr.Id, channel.ChannelId)
							} else {
								//log.Printf("Not showing a reply TO our user %s FROM someone else (this is actually a reply to their own original reply) for %s\n", usr.Id, channel.ChannelId)
							}
						}
					} else {
						log.Printf("Did not have any tweets in our cache from this user which likely means they are responding to themselves responding to someone who isn't showing replies: %s", tweet.IDStr)
					}

				} else {
					//Not sure how we would get here, this is a reply but it's not from us or to us??
					log.Printf("ERROR: Somehow we got a tweet which is a reply that is not from us or to us.... %+v\n", tweet)
				}
			}
		}

	} else {
		//Not a retweet, or a reply, just a normal tweet
		tweetType = TweetType(CREATED_BY_USER)
		log.Info().Str("sender", tweet.User.ScreenName).Str("tweetId", tweet.IDStr).Bool("fromSender", true).Msgf(tweetFormat, tweet.User.ScreenName, tweetText)
		if usr, ok := th.userMap[tweet.User.IDStr]; ok {
			for _, channel := range usr.Channels {
				if channel.DisplayFromUser {
					log.Printf("Showing a tweet FROM our user: %s for %s", usr.Id, channel.ChannelId)
					channelMap[channel.ChannelId] = struct{}{}
				} else {
					//log.Printf("Not Showing a tweet FROM our user: %s for %s", usr.Id, channel.ChannelId)
				}
			}
		} else {
			log.Printf("ERROR: Somehow we got a tweet which is not a retweet or reply but also not from one of our users.... %+v\n", tweet)
		}

	}
	if tweetType == NOT_DEFINED {
		spew.Dump(tweet)
	}

	return channelMap, tweetType
}

func (th *twitterHandler) getOriginalUser(id int64) *twitterUser {
	var currUser string
	var currId int64
	currUser = ""
	currId = id
	for currId != -1 {
		if usr, ok := th.userMap[string(currUser)]; ok {
			return usr
		}
		currId, currUser = th.getReplyTo(currId)
	}
	return nil
}

func (th *twitterHandler) getReplyTo(id int64) (int64, string) {
	if th.tweetCache == nil {
		return -1, ""
	}
	if val, ok := th.tweetCache.GetIfPresent(id); ok {
		return val.(*twitter.Tweet).InReplyToStatusID, val.(*twitter.Tweet).InReplyToUserIDStr
	} else {
		return -1, ""
	}
}

func (th *twitterHandler) getOriginalTweet(id string) *twitter.Tweet {
	twt := &twitter.Tweet{}
	idBytes := []byte(id)
	if ok := th.allCache.Has(idBytes); ok {
		val := th.allCache.Get(nil, idBytes)
		err := json.Unmarshal(val, twt)
		if err != nil {
			log.Error().Err(err).Msg("Failed to lookup tweet, could not unmarshal json")
			return nil
		}
	} else {
		log.Printf("Tweet cache miss for %v, looking up tweet", id)
		boolFalse := false
		n, err := strconv.ParseInt(id, 10, 64)
		if err != nil {
			log.Error().Err(err).Msg("Failed to lookup tweet, ID wouldn't convert to int")
			return nil
		}
		tweet, _, err := th.twitterClient.Statuses.Show(n, &twitter.StatusShowParams{
			IncludeMyRetweet: &boolFalse,
		})
		if err != nil {
			log.Error().Err(err).Msg("Failed to lookup tweet when looking for original tweet in reply chain")
			return nil
		}
		ts, err := json.Marshal(tweet)
		if err != nil {
			log.Error().Err(err).Msg("Failed to marshal tweet and put it in cache")
		} else {
			th.allCache.Set(idBytes, ts)
		}
		twt = tweet
	}
	if _, ok := th.userMap[twt.User.IDStr]; twt.InReplyToStatusIDStr == "" && ok {
		return twt
	} else {
		nextId := twt.InReplyToStatusIDStr
		if nextId == id {
			log.Error().Msg("Infinite recursion detected getting original tweet")
			return nil
		}
		if twt.InReplyToStatusIDStr == "" {
			//Likely a Manual Reply, where the tweet starts with @user in our list but doesn't reference an existing tweet, we ignore these
			return nil
		}
		return th.getOriginalTweet(twt.InReplyToStatusIDStr)
	}

}
