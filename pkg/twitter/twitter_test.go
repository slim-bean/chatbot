package twitter

import (
	"github.com/dghubble/go-twitter/twitter"
	"github.com/goburrow/cache"
	"testing"
	"time"
)

func TestProcessTweetCreatedByUser(t *testing.T) {
	tweet := &twitter.Tweet{
		User: &twitter.User{
			IDStr: "1",
		},
	}
	userMap := make(map[string]*twitterUser)
	userMap["1"] = &twitterUser{
		ScreenName: "",
		Id:         "1",
		Channels: []channelOptions{
			{
				ChannelId:                        "A",
				DisplayFromUser:                  true,
				DisplayRetweetsOfOtherUserTweets: false,
				DisplayOtherUserRetweetOfUs:      false,
				DisplayMessagesToOtherUsers:      false,
				DisplayMessagesFromOtherUsers:    false,
			},
			{
				ChannelId:                        "B",
				DisplayFromUser:                  false,
				DisplayRetweetsOfOtherUserTweets: false,
				DisplayOtherUserRetweetOfUs:      false,
				DisplayMessagesToOtherUsers:      false,
				DisplayMessagesFromOtherUsers:    false,
			},
		},
	}
	th := New(nil, nil, userMap)
	result, tweetType := th.ProcessTweet(tweet)
	if len(result) != 1 {
		t.Error()
	}
	if _, ok := result["A"]; !ok {
		t.Error()
	}
	if tweetType != CREATED_BY_USER {
		t.Error()
	}
}

func TestProcessRetweetCreatedByUser(t *testing.T) {
	tweet := &twitter.Tweet{
		RetweetedStatus: &twitter.Tweet{},
		User: &twitter.User{
			IDStr: "1",
		},
	}
	userMap := make(map[string]*twitterUser)
	userMap["1"] = &twitterUser{
		ScreenName: "",
		Id:         "1",
		Channels: []channelOptions{
			{
				ChannelId:                        "A",
				DisplayFromUser:                  false,
				DisplayRetweetsOfOtherUserTweets: true,
				DisplayOtherUserRetweetOfUs:      false,
				DisplayMessagesToOtherUsers:      false,
				DisplayMessagesFromOtherUsers:    false,
			},
			{
				ChannelId:                        "B",
				DisplayFromUser:                  false,
				DisplayRetweetsOfOtherUserTweets: false,
				DisplayOtherUserRetweetOfUs:      false,
				DisplayMessagesToOtherUsers:      false,
				DisplayMessagesFromOtherUsers:    false,
			},
		},
	}
	th := New(nil, nil, userMap)
	result, tweetType := th.ProcessTweet(tweet)
	if len(result) != 1 {
		t.Error()
	}
	if _, ok := result["A"]; !ok {
		t.Error()
	}
	if tweetType != RETWEET_CREATED_BY_USER {
		t.Error()
	}
}

func TestProcessRetweetOfTweetCreatedByUser(t *testing.T) {
	tweet := &twitter.Tweet{
		RetweetedStatus: &twitter.Tweet{
			User: &twitter.User{
				IDStr: "1",
			},
		},
		User: &twitter.User{
			IDStr: "2",
		},
	}
	userMap := make(map[string]*twitterUser)
	userMap["1"] = &twitterUser{
		ScreenName: "",
		Id:         "1",
		Channels: []channelOptions{
			{
				ChannelId:                        "A",
				DisplayFromUser:                  false,
				DisplayRetweetsOfOtherUserTweets: false,
				DisplayOtherUserRetweetOfUs:      true,
				DisplayMessagesToOtherUsers:      false,
				DisplayMessagesFromOtherUsers:    false,
			},
			{
				ChannelId:                        "B",
				DisplayFromUser:                  false,
				DisplayRetweetsOfOtherUserTweets: false,
				DisplayOtherUserRetweetOfUs:      false,
				DisplayMessagesToOtherUsers:      false,
				DisplayMessagesFromOtherUsers:    false,
			},
		},
	}
	th := New(nil, nil, userMap)
	result, tweetType := th.ProcessTweet(tweet)
	if len(result) != 1 {
		t.Error()
	}
	if _, ok := result["A"]; !ok {
		t.Error()
	}
	if tweetType != RETWEET_OF_TWEET_CREATED_BY_USER {
		t.Error()
	}
}

func TestProcessReplyFromOurUserToOtherUsers(t *testing.T) {
	tweet := &twitter.Tweet{
		InReplyToUserIDStr: "2",
		User: &twitter.User{
			IDStr: "1",
		},
	}
	userMap := make(map[string]*twitterUser)
	userMap["1"] = &twitterUser{
		ScreenName: "",
		Id:         "1",
		Channels: []channelOptions{
			{
				ChannelId:                        "A",
				DisplayFromUser:                  false,
				DisplayRetweetsOfOtherUserTweets: false,
				DisplayOtherUserRetweetOfUs:      false,
				DisplayMessagesToOtherUsers:      true,
				DisplayMessagesFromOtherUsers:    false,
			},
			{
				ChannelId:                        "B",
				DisplayFromUser:                  false,
				DisplayRetweetsOfOtherUserTweets: false,
				DisplayOtherUserRetweetOfUs:      false,
				DisplayMessagesToOtherUsers:      false,
				DisplayMessagesFromOtherUsers:    false,
			},
		},
	}
	th := New(nil, nil, userMap)
	result, tweetType := th.ProcessTweet(tweet)
	if len(result) != 1 {
		t.Error()
	}
	if _, ok := result["A"]; !ok {
		t.Error()
	}
	if tweetType != REPLY_TO_OTHER_USER {
		t.Error()
	}
}

func TestProcessReplyFromOurUserToOurself(t *testing.T) {

	//Test reply to ourselves
	tweet := &twitter.Tweet{
		InReplyToUserIDStr: "1",
		User: &twitter.User{
			IDStr: "1",
		},
	}
	userMap := make(map[string]*twitterUser)
	userMap["1"] = &twitterUser{
		ScreenName: "",
		Id:         "1",
		Channels: []channelOptions{
			{
				ChannelId:                        "A",
				DisplayFromUser:                  true,
				DisplayRetweetsOfOtherUserTweets: false,
				DisplayOtherUserRetweetOfUs:      false,
				DisplayMessagesToOtherUsers:      false,
				DisplayMessagesFromOtherUsers:    false,
			},
			{
				ChannelId:                        "B",
				DisplayFromUser:                  false,
				DisplayRetweetsOfOtherUserTweets: false,
				DisplayOtherUserRetweetOfUs:      false,
				DisplayMessagesToOtherUsers:      false,
				DisplayMessagesFromOtherUsers:    false,
			},
		},
	}
	th := New(nil, nil, userMap)
	result, _ := th.ProcessTweet(tweet)
	if len(result) != 1 {
		t.Error()
	}
	if _, ok := result["A"]; !ok {
		t.Error()
	}
}

func TestProcessReplyFromOtherUsersToOurUser(t *testing.T) {
	tweet := &twitter.Tweet{
		InReplyToUserIDStr: "1",
		User: &twitter.User{
			IDStr: "2",
		},
	}
	userMap := make(map[string]*twitterUser)
	userMap["1"] = &twitterUser{
		ScreenName: "",
		Id:         "1",
		Channels: []channelOptions{
			{
				ChannelId:                        "A",
				DisplayFromUser:                  false,
				DisplayRetweetsOfOtherUserTweets: false,
				DisplayOtherUserRetweetOfUs:      false,
				DisplayMessagesToOtherUsers:      false,
				DisplayMessagesFromOtherUsers:    true,
			},
			{
				ChannelId:                        "B",
				DisplayFromUser:                  false,
				DisplayRetweetsOfOtherUserTweets: false,
				DisplayOtherUserRetweetOfUs:      false,
				DisplayMessagesToOtherUsers:      false,
				DisplayMessagesFromOtherUsers:    false,
			},
		},
	}
	th := New(nil, nil, userMap)
	result, tweetType := th.ProcessTweet(tweet)
	if len(result) != 1 {
		t.Error()
	}
	if _, ok := result["A"]; !ok {
		t.Error()
	}
	if tweetType != REPLY_TO_TWEET_CREATED_BY_USER {
		t.Error()
	}
}

func TestProcessReplyFromOtherUserToThemselvesToOurUser(t *testing.T) {
	originalTweet := &twitter.Tweet{
		ID: 1,
		User: &twitter.User{
			IDStr: "1",
		},
	}
	replyTweet1 := &twitter.Tweet{
		InReplyToUserIDStr: "1",
		InReplyToStatusID:  1,
		ID:                 2,
		User: &twitter.User{
			IDStr: "2",
		},
	}
	replyTweet2 := &twitter.Tweet{
		InReplyToUserIDStr: "2",
		InReplyToStatusID:  2,
		ID:                 3,
		User: &twitter.User{
			IDStr: "2",
		},
	}

	load := func(k cache.Key) (cache.Value, error) {
		return nil, nil
	}
	// Create a new cache
	tweetCache := cache.NewLoadingCache(load,
		cache.WithMaximumSize(1000),
		cache.WithExpireAfterWrite(48*time.Hour),
	)

	tweetCache.Put(originalTweet.ID, originalTweet)
	tweetCache.Put(replyTweet1.ID, replyTweet1)
	tweetCache.Put(replyTweet2.ID, replyTweet2)

	//Apparently there is some slowness to loading the cache so we want to poll to wait for it to load
	wait := true
	for wait {
		if _, ok := tweetCache.GetIfPresent(replyTweet2.ID); ok {
			wait = false
		}
	}

	userMap := make(map[string]*twitterUser)
	userMap["1"] = &twitterUser{
		ScreenName: "",
		Id:         "1",
		Channels: []channelOptions{
			{
				ChannelId:                        "A",
				DisplayFromUser:                  false,
				DisplayRetweetsOfOtherUserTweets: false,
				DisplayOtherUserRetweetOfUs:      false,
				DisplayMessagesToOtherUsers:      false,
				DisplayMessagesFromOtherUsers:    true,
			},
			{
				ChannelId:                        "B",
				DisplayFromUser:                  false,
				DisplayRetweetsOfOtherUserTweets: false,
				DisplayOtherUserRetweetOfUs:      false,
				DisplayMessagesToOtherUsers:      false,
				DisplayMessagesFromOtherUsers:    false,
			},
		},
	}
	th := New(nil, tweetCache, userMap)
	result, tweetType := th.ProcessTweet(replyTweet2)
	if len(result) != 1 {
		t.Error()
	}
	if _, ok := result["A"]; !ok {
		t.Error()
	}
	if tweetType != REPLY_TO_TWEET_CREATED_BY_USER {
		t.Error()
	}

	//Using a tweet which is replying to a status not in our cache, to validate this path works as well
	//Messages should not be cached unless a previous processEvent allowed them to be
	replyTweet3 := &twitter.Tweet{
		InReplyToUserIDStr: "2",
		InReplyToStatusID:  200,
		ID:                 4,
		User: &twitter.User{
			IDStr: "2",
		},
	}
	result, tweetType = th.ProcessTweet(replyTweet3)
	if len(result) != 0 {
		t.Error()
	}
	if tweetType != REPLY_TO_TWEET_CREATED_BY_USER {
		t.Error()
	}
}

func Test_getOriginalTweet(t *testing.T) {
	originalTweet := &twitter.Tweet{
		ID:    1,
		IDStr: "1",
		User: &twitter.User{
			IDStr: "1",
		},
	}
	replyTweet1 := &twitter.Tweet{
		InReplyToUserIDStr:   "1",
		InReplyToStatusID:    1,
		InReplyToStatusIDStr: "1",
		ID:                   2,
		IDStr:                "2",
		User: &twitter.User{
			IDStr: "2",
		},
	}
	replyTweet2 := &twitter.Tweet{
		InReplyToUserIDStr:   "2",
		InReplyToStatusID:    2,
		InReplyToStatusIDStr: "2",
		ID:                   3,
		IDStr:                "3",
		User: &twitter.User{
			IDStr: "2",
		},
	}

	userMap := make(map[string]*twitterUser)
	userMap["1"] = &twitterUser{
		ScreenName: "",
		Id:         "1",
	}

	th := New(nil, nil, userMap)

	//Calling process on these just to load them in the cache
	th.ProcessTweet(originalTweet)
	th.ProcessTweet(replyTweet1)
	th.ProcessTweet(replyTweet2)

	tweet := th.getOriginalTweet("3")
	if tweet.ID != 1 {
		t.Fail()
	}
}

func Test_getOriginalTweet_ReplyToSelf(t *testing.T) {
	originalTweet := &twitter.Tweet{
		ID:    1,
		IDStr: "1",
		User: &twitter.User{
			IDStr: "1",
		},
	}
	replyTweet1 := &twitter.Tweet{
		InReplyToUserIDStr:   "1",
		InReplyToStatusID:    1,
		InReplyToStatusIDStr: "1",
		ID:                   2,
		IDStr:                "2",
		User: &twitter.User{
			IDStr: "1",
		},
	}
	replyTweet2 := &twitter.Tweet{
		InReplyToUserIDStr:   "2",
		InReplyToStatusID:    2,
		InReplyToStatusIDStr: "2",
		ID:                   3,
		IDStr:                "3",
		User: &twitter.User{
			IDStr: "1",
		},
	}

	userMap := make(map[string]*twitterUser)
	userMap["1"] = &twitterUser{
		ScreenName: "",
		Id:         "1",
	}

	th := New(nil, nil, userMap)

	//Calling process on these just to load them in the cache
	th.ProcessTweet(originalTweet)
	th.ProcessTweet(replyTweet1)
	th.ProcessTweet(replyTweet2)

	tweet := th.getOriginalTweet("3")
	if tweet.ID != 1 {
		t.Fail()
	}
}
