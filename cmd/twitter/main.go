package main

import (
	"encoding/json"
	"flag"
	"github.com/coreos/pkg/flagutil"
	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
	"github.com/goburrow/cache"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/yosssi/gmq/mqtt"
	"github.com/yosssi/gmq/mqtt/client"
	"net/http"
	_ "net/http/pprof"
	"oqqer.com/chatbot/pkg/chatlib"
	twit "oqqer.com/chatbot/pkg/twitter"
	"os"
	"strings"
	"time"
)

var ()

func main() {
	flags := flag.NewFlagSet("user-auth", flag.ExitOnError)
	consumerKey := flags.String("consumer-key", "", "Twitter Consumer Key")
	consumerSecret := flags.String("consumer-secret", "", "Twitter Consumer Secret")
	accessToken := flags.String("access-token", "", "Twitter Access Token")
	accessSecret := flags.String("access-secret", "", "Twitter Access Secret")
	debug := flags.Bool("debug", false, "sets log level to debug")
	flags.Parse(os.Args[1:])
	flagutil.SetFlagsFromEnv(flags, "TWITTER")

	if *consumerKey == "" || *consumerSecret == "" || *accessToken == "" || *accessSecret == "" {
		panic("Consumer key/secret and Access token/secret required")
	}

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	// Default level for this example is info, unless debug flag is present
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if *debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	log.Debug().Msg("This message appears only when log level set to Debug")
	log.Info().Msg("This message appears when log level set to Debug or Info")

	log.Info().Msg("Connecting to mqtt")
	//Create the twitterClient
	cli, err := chatlib.Connect("fu")
	if err != nil {
		panic(err)
	}
	log.Info().Msg("Connected to mqtt")

	log.Info().Msg("Starting HTTP Server")
	http.Handle("/metrics", promhttp.Handler())
	go func() {
		err := http.ListenAndServe(":6060", nil)
		if err != nil {
			log.Error().Err(err).Msg("Failed to start HTTP server")
		}
	}()

	log.Info().Msg("Getting twitter users")
	config := oauth1.NewConfig(*consumerKey, *consumerSecret)
	token := oauth1.NewToken(*accessToken, *accessSecret)
	// OAuth1 http.Client will automatically authorize Requests
	httpClient := config.Client(oauth1.NoContext, token)

	// Twitter Client
	twitterClient := twitter.NewClient(httpClient)

	load := func(k cache.Key) (cache.Value, error) {
		log.Printf("Tweet cache miss for %d, looking up tweet", k.(int64))
		boolFalse := false
		tweet, _, err := twitterClient.Statuses.Show(k.(int64), &twitter.StatusShowParams{
			IncludeMyRetweet: &boolFalse,
		})
		if err != nil {
			return nil, err
		}
		return tweet, nil
	}
	// Create a new cache
	tweetCache := cache.NewLoadingCache(load,
		cache.WithMaximumSize(1000),
		cache.WithExpireAfterWrite(48*time.Hour),
	)

	userMap, err := twit.LoadFromFile("/config/twitter.json")
	if err != nil {
		panic(err)
	}

	twitterHandler := twit.New(twitterClient, tweetCache, userMap)

	var usersIds []string
	channelTopics := make(map[string][]string)

	for id, user := range userMap {
		usersIds = append(usersIds, id)
		for _, channel := range user.Channels {
			log.Printf("Adding %s to channel %s\n", user.ScreenName, channel.ChannelId)
			channelTopics[channel.ChannelId] = append(channelTopics[channel.ChannelId], user.ScreenName)
		}
	}

	for channel, users := range channelTopics {

		log.Printf("Sending topic for Channel %s\n", channel)
		//Post the topic which sets the list of users
		//Create the response wrapper
		topicMessage := new(chatlib.TopicMessage)
		topicMessage.Channel = channel
		topicString := "Following: "
		for index, element := range users {
			topicString = topicString + "@" + element
			if index < len(users)-1 {
				topicString = topicString + ", "
			}
		}
		topicMessage.Topic = topicString

		strMsg, err := json.Marshal(topicMessage)
		if err != nil {
			log.Error().Err(err)
		}

		//Send it to the MQTT queue
		err = cli.Publish(&client.PublishOptions{
			QoS:       mqtt.QoS2,
			TopicName: []byte("slack/topic"),
			Message:   []byte(strMsg),
		})
		if err != nil {
			log.Error().Err(err)
		}

	}

	// Listen for action events and handle them
	ignoreMap := make(map[string]struct{})
	topicHandler := func(topicName, message []byte) {
		var m chatlib.StopFollowingThreadAction
		err := json.Unmarshal(message, &m)
		if err != nil {
			log.Error().Err(err).Msg("Error unmarshalling StopFollowingThreadAction")
			return
		}
		log.Info().Str("statusId", m.StatusId).Msg("Ignoring replies to tweet statusId")
		ignoreMap[m.StatusId] = struct{}{}
		//Create the response wrapper
		response := new(chatlib.PostMessage)
		response.Channel = m.Channel
		response.Text = "Now ignoring all replies to Status: " + m.StatusId

		strMsg, err := json.Marshal(response)
		if err != nil {
			log.Error().Err(err).Msg("Failed to marshal action event response")
			return
		}

		//Send it to the MQTT queue
		err = cli.Publish(&client.PublishOptions{
			QoS:       mqtt.QoS2,
			TopicName: []byte("slack/apipost"),
			Message:   []byte(strMsg),
		})
		if err != nil {
			log.Error().Err(err).Msg("Failed to send reply to action event")
		}
	}
	err = chatlib.Subscribe(cli, "twitter/incoming", topicHandler)
	if err != nil {
		panic(err)
	}

	//TODO
	// URL expansion

	//log.Println(usersIds)

	// Convenience Demux demultiplexed stream messages
	demux := twitter.NewSwitchDemux()
	demux.Tweet = func(tweet *twitter.Tweet) {
		//start := time.Now()
		var tweetText string
		if tweet.ExtendedTweet != nil {
			tweetText = tweet.ExtendedTweet.FullText
		} else {
			tweetText = tweet.Text
		}

		//log.Printf("%#v\n", tweet)
		if tweet.RetweetedStatus == nil {
			//fmt.Printf("T: %s, F: %s, M: %s\n", tweet.InReplyToScreenName, tweet.User.ScreenName, tweet.Text)
			//log.Info().Str("sender", tweet.User.ScreenName).Str("tweetId", tweet.IDStr).Str("replyTo", tweet.InReplyToScreenName).Str("replyToTweet", tweet.InReplyToStatusIDStr).Msg(tweetText)
		}

		channels, _ := twitterHandler.ProcessTweet(tweet)

		//Cache this for replyto's
		//Note there are some assumptions made around tweets in this cache only being here after passing the processTweet method
		//If this is changed, look at how getOriginalUser is used and how it works
		tweetCache.Put(tweet.ID, tweet)

		if len(channels) == 0 {
			//elapsed := time.Since(start)
			//log.Printf("Processing not-shown tweet took %s", elapsed)
			return
		}

		//Create the response
		attachment := new(chatlib.Attachment)
		attachment.Color = "#1DA1F2"
		attachment.AuthorIcon = tweet.User.ProfileImageURLHttps
		attachment.AuthorLink = "https://twitter.com/" + tweet.User.ScreenName
		attachment.AuthorName = tweet.User.Name + " (@" + tweet.User.ScreenName + ")"
		attachment.Text = tweetText

		if strings.HasPrefix(attachment.Text, "Accident of motor vehicles involving unknown injury") {
			log.Debug().Msg("Not showing a tweet for Accident with unknown injury")
			return
		}

		//From our user, check for color
		if user, ok := userMap[tweet.User.IDStr]; ok {
			if user.Color != "" {
				attachment.Color = user.Color
			}
		}

		//If this is a reply we include the tweet being replied to as a footer
		if tweet.InReplyToStatusID != 0 {

			if _, ok := ignoreMap[tweet.InReplyToStatusIDStr]; ok {
				log.Printf("Ignoring a reply to StatusID: %s", tweet.InReplyToStatusIDStr)
				return
			}

			replyto, err := tweetCache.Get(tweet.InReplyToStatusID)
			if err != nil {
				log.Printf("Error looking up tweet: %+v", err)
			} else {
				attachment.AuthorName = attachment.AuthorName + " -> " + replyto.(*twitter.Tweet).User.Name + " (@" + replyto.(*twitter.Tweet).User.ScreenName + ")"
				attachment.Footer = replyto.(*twitter.Tweet).Text
				attachment.FooterIcon = replyto.(*twitter.Tweet).User.ProfileImageURLHttps
				//Check to see if we can apply a color
				if user, ok := userMap[replyto.(*twitter.Tweet).User.IDStr]; ok {
					if user.Color != "" {
						attachment.Color = user.Color
					}
				}
				//We are abusing this field so that our callbacks to stop following a thread can easily do so
				attachment.Fallback = tweet.InReplyToStatusIDStr
			}

		}

		//If this is quoting another ID, show what was quoted
		if tweet.QuotedStatusID != 0 {
			quoted, err := tweetCache.Get(tweet.QuotedStatusID)
			if err != nil {
				log.Printf("Error looking up tweet: %+v", err)
			} else {
				quotedTweet := quoted.(*twitter.Tweet)
				attachment.MarkdownIn = []string{"text"}
				var text string
				if quotedTweet.ExtendedTweet != nil {
					text = quotedTweet.ExtendedTweet.FullText
				} else {
					text = quotedTweet.Text
				}
				attachment.Text = attachment.Text + "\n>>> " + text
				attachment.Footer = quotedTweet.User.Name + " (@" + quotedTweet.User.ScreenName + ") https://twitter.com/statuses/" + quotedTweet.IDStr
				attachment.FooterIcon = quotedTweet.User.ProfileImageURLHttps
				//Check to see if we can apply a color
				if user, ok := userMap[quotedTweet.User.IDStr]; ok {
					if user.Color != "" {
						attachment.Color = user.Color
					}
				}
			}
		}

		if tweet.ExtendedEntities != nil && len(tweet.ExtendedEntities.Media) > 0 {
			entity := tweet.ExtendedEntities.Media[0]
			if entity.Type == "photo" {
				attachment.Image = entity.MediaURLHttps
			}
		} else if tweet.Entities != nil && len(tweet.Entities.Media) > 0 {
			//I don't think we need this fallback anymore based on the docs, but leaving it for now, if there is media
			//it's always supposed to be in the ExtendedEntities section
			attachment.Image = tweet.Entities.Media[0].MediaURLHttps
		}

		for channel := range channels {
			//Create the response wrapper
			apiMessage := new(chatlib.PostMessage)
			apiMessage.Channel = channel
			apiMessage.Attachments = []chatlib.Attachment{*attachment}

			strMsg, err := json.Marshal(apiMessage)
			if err != nil {
				log.Error().Err(err)
			}

			//Send it to the MQTT queue
			err = cli.Publish(&client.PublishOptions{
				QoS:       mqtt.QoS0,
				TopicName: []byte("slack/apipost"),
				Message:   []byte(strMsg),
			})
			if err != nil {
				log.Error().Err(err)
			}
		}

		//elapsed := time.Since(start)
		//log.Printf("Processing tweet took %s", elapsed)
	}
	demux.DM = func(dm *twitter.DirectMessage) {
		//fmt.Println(dm.SenderID)
	}
	demux.Event = func(event *twitter.Event) {
		//fmt.Printf("%#v\n", event)
	}

	log.Info().Msg("Starting Stream...")

	// FILTER
	filterParams := &twitter.StreamFilterParams{
		Follow:        usersIds,
		StallWarnings: twitter.Bool(true),
	}
	stream, err := twitterClient.Streams.Filter(filterParams)
	if err != nil {
		panic(err)
	}

	// Receive messages until stopped or stream quits
	demux.HandleChan(stream.Messages)

	//// Wait for SIGINT and SIGTERM (HIT CTRL-C)
	//ch := make(chan os.Signal)
	//signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	//log.Info().Str("signal", (<-ch).String()).Msg("Received Signal")

	log.Info().Msg("Handle Chan exited. Stopping Stream...")
	stream.Stop()

	log.Info().Msg("Exiting")

}
