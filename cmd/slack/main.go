package main

import (
	"bytes"
	"crypto/x509"
	"encoding/asn1"
	"encoding/json"
	"encoding/pem"
	"flag"
	"fmt"
	"github.com/nlopes/slack"
	"github.com/nlopes/slack/slackevents"
	"github.com/pkg/errors"
	"github.com/yosssi/gmq/mqtt"
	"github.com/yosssi/gmq/mqtt/client"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"oqqer.com/chatbot/pkg/chatlib"
	"os"
	"os/signal"
	"strings"
)

func main() {
	// Set up channel on which to send signal notifications.
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, os.Interrupt, os.Kill)

	flags := flag.NewFlagSet("user-auth", flag.ExitOnError)
	botToken := flags.String("bot-oauth", "", "Slack Bot OAuth Token")
	userToken := flags.String("user-oauth", "", "Slack User OAuth Token")
	signingToken := flags.String("signing-token", "", "Token to check requests from slack")
	flags.Parse(os.Args[1:])
	//flagutil.SetFlagsFromEnv(flags, "TWITTER")

	if *botToken == "" || *userToken == "" {
		log.Fatal("Bot and User OAuth Slack Tokens Required")
	}

	if *signingToken == "" {
		log.Fatal("Signing token is required")
	}

	// Create an MQTT Client.
	cli := client.New(&client.Options{
		// Define the processing of the error handler.
		ErrorHandler: func(err error) {
			fmt.Println(err)
		},
	})

	// Terminate the Client.
	defer cli.Terminate()

	// Connect to the MQTT Server.
	err := cli.Connect(&client.ConnectOptions{
		Network:  "tcp",
		Address:  "mqtt:1883",
		ClientID: []byte("wendell-slack"),
	})
	if err != nil {
		panic(err)
	}

	//Init the slack client
	api := slack.New(
		*botToken,
		slack.OptionDebug(true),
		slack.OptionLog(log.New(os.Stdout, "slack-bot: ", log.Lshortfile|log.LstdFlags)),
	)

	//Create a go routine for the rtm connection manager and to listen for messages
	rtm := api.NewRTM()
	go rtm.ManageConnection()
	go receiveMessages(rtm, cli)

	//Create a message handler for messages we need to send back to slack
	handler := func(topicName, message []byte) {
		var m chatlib.Message
		err := json.Unmarshal(message, &m)
		if err != nil {
			log.Println("Message unmarshal error: ", err)
			return
		}
		log.Printf("Message: %+v\n", m)
		rtm.SendMessage(convertSlackMessage(rtm, &m))
	}
	//Subscribe and listen for messages
	err = chatlib.Subscribe(cli, "slack/outgoing", handler)
	if err != nil {
		panic(err)
	}

	//Create a message handler for messages we need to send back to slack
	postHandler := func(topicName, message []byte) {

		req, err := http.NewRequest("POST", "https://slack.com/api/chat.postMessage", bytes.NewBuffer(message))
		req.Header.Set("Authorization", "Bearer "+*botToken)
		req.Header.Set("Content-Type", "application/json")

		httpClient := &http.Client{}
		resp, err := httpClient.Do(req)

		if err != nil {
			log.Println(err)
		}
		log.Printf("Post Response: %+v\n", resp)
		if resp.Body != nil {
			defer resp.Body.Close()
			body, _ := ioutil.ReadAll(resp.Body)
			log.Println(string(body))
		}
	}
	//Subscribe and listen for messages
	err = chatlib.Subscribe(cli, "slack/apipost", postHandler)
	if err != nil {
		panic(err)
	}

	//Create a message handler for messages we need to send back to slack
	topicHandler := func(topicName, message []byte) {
		log.Printf("Received Topic Request for Channel")

		req, err := http.NewRequest("POST", "https://slack.com/api/channels.setTopic", bytes.NewBuffer(message))
		req.Header.Set("Authorization", "Bearer "+*userToken)
		req.Header.Set("Content-Type", "application/json")

		httpClient := &http.Client{}
		resp, err := httpClient.Do(req)

		if err != nil {
			log.Println(err)
		}
		log.Printf("Topic Response: %+v\n", resp)
		if resp.Body != nil {
			defer resp.Body.Close()
			body, _ := ioutil.ReadAll(resp.Body)
			log.Println(string(body))
		}

	}
	//Subscribe and listen for messages
	err = chatlib.Subscribe(cli, "slack/topic", topicHandler)
	if err != nil {
		panic(err)
	}

	http.HandleFunc("/slack/test", func(w http.ResponseWriter, r *http.Request) {
		err := validateClientCert(r.Header.Get("X-Client-Certificate"))
		if err != nil {
			log.Println("Failed to auth client: ", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Unauthorized"))
			return
		}
		fmt.Fprintf(w, "Hello, you've requested: %s\n", r.URL.Path)
	})

	http.HandleFunc("/slack/actions-endpoint", func(w http.ResponseWriter, r *http.Request) {
		err := validateClientCert(r.Header.Get("X-Client-Certificate"))
		if err != nil {
			log.Println("Failed to auth client: ", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Unauthorized"))
			return
		}
		buf := new(bytes.Buffer)
		buf.ReadFrom(r.Body)
		body := buf.String()
		verifier, err := slack.NewSecretsVerifier(r.Header, *signingToken)
		if err != nil {
			log.Println("Failed to create a secrets verifier:", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Unauthorized"))
			return
		}
		io.WriteString(&verifier, body)
		err = verifier.Ensure()
		if err != nil {
			log.Println("Failed to verify message from slack: ", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Unauthorized"))
			return
		}
		unescaped, err := url.QueryUnescape(body)
		if err != nil {
			log.Println("Failed to unescape body: ", err)
			return
		}
		log.Println("Action: ", unescaped)
		actionEvent, err := slackevents.ParseActionEvent(strings.Replace(unescaped, "payload=", "", 1), slackevents.OptionNoVerifyToken())
		if err != nil {
			log.Println("Error processing action event, being ignored: ", err)
			return
		}
		switch actionEvent.CallbackID {
		case "twitter_ignore_thread":
			if len(actionEvent.Message.Attachments) == 1 {
				strMsg, err := json.Marshal(chatlib.StopFollowingThreadAction{
					StatusId: actionEvent.Message.Attachments[0].Fallback,
					Channel:  actionEvent.Channel.ID,
				})
				if err != nil {
					log.Println("Failed marshall action message for twitter action:", err)
					return
				}
				err = cli.Publish(&client.PublishOptions{
					QoS:       mqtt.QoS2,
					TopicName: []byte("twitter/incoming"),
					Message:   []byte(strMsg),
				})
				if err != nil {
					log.Println("Failed to send action message to twitter module", err)
				}
			}
		}
	})

	http.HandleFunc("/slack/events-endpoint", func(w http.ResponseWriter, r *http.Request) {
		err := validateClientCert(r.Header.Get("X-Client-Certificate"))
		if err != nil {
			log.Println("Failed to auth client: ", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Unauthorized"))
			return
		}
		buf := new(bytes.Buffer)
		buf.ReadFrom(r.Body)
		body := buf.String()
		verifier, err := slack.NewSecretsVerifier(r.Header, *signingToken)
		if err != nil {
			log.Println("Failed to create a secrets verifier:", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Unauthorized"))
			return
		}
		io.WriteString(&verifier, body)
		err = verifier.Ensure()
		if err != nil {
			log.Println("Failed to verify message from slack: ", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Unauthorized"))
			return
		}
		eventsAPIEvent, e := slackevents.ParseEvent(json.RawMessage(body), slackevents.OptionNoVerifyToken())
		if e != nil {
			log.Println("Error parsing event from slack: ", e)
			w.WriteHeader(http.StatusInternalServerError)
		}

		if eventsAPIEvent.Type == slackevents.URLVerification {
			var r *slackevents.ChallengeResponse
			err := json.Unmarshal([]byte(body), &r)
			if err != nil {
				log.Println("Error unmarshalling event from slack: ", e)
				w.WriteHeader(http.StatusInternalServerError)
			}
			w.Header().Set("Content-Type", "text")
			w.Write([]byte(r.Challenge))
		}
		if eventsAPIEvent.Type == slackevents.CallbackEvent {
			innerEvent := eventsAPIEvent.InnerEvent
			switch ev := innerEvent.Data.(type) {
			case *slackevents.AppMentionEvent:
				api.PostMessage(ev.Channel, slack.MsgOptionText("Yes, hello.", false))
			}
		}
	})

	go func() {
		log.Println("Server listening On 3000")
		err := http.ListenAndServe(":3000", nil)
		if err != nil {
			panic("ListenAndServe: " + err.Error())
		}
	}()

	// Wait for a signal (shutdown/interrupt)
	<-sigc
	log.Println("Shutting Down SlackBot")
	rtm.Disconnect()
}

func validateClientCert(escapedCertHeader string) error {
	unescaped, err := url.QueryUnescape(escapedCertHeader)
	if err != nil {
		return errors.Wrap(err, "Failed to URL Decode PEM")
	}
	block, _ := pem.Decode([]byte(unescaped))
	if block == nil {
		return errors.New("Did not decode any PEM cert info from header")
	}
	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return errors.Wrap(err, "Failed to extract cert from pem data")
	}

	for _, ext := range cert.Extensions {
		if ext.Id.Equal([]int{2, 5, 29, 17}) {
			//This isn't great, I copied most of this from x509.go::forEachSAN, I also pulled that ASN identifier above from that file
			var seq asn1.RawValue
			rest, err := asn1.Unmarshal(ext.Value, &seq)
			if err != nil {
				return errors.Wrap(err, "Failed to parse initial part of extension")
			} else if len(rest) != 0 {
				return errors.New("x509: trailing data after X.509 extension")
			}
			if !seq.IsCompound || seq.Tag != 16 || seq.Class != 0 {
				return errors.New("Bad SAN structure")
			}
			//The code in forEachSAN loops over the "rest" but since i'm looking for very specific data I didn't do that
			rest = seq.Bytes
			var v asn1.RawValue
			_, err = asn1.Unmarshal(rest, &v)
			if err != nil {
				return errors.Wrap(err, "Failed to unmarshal asn1 SAN field")
			}
			san := string(v.Bytes)
			tag := v.Tag
			if tag == 2 && san == "platform-tls-client.slack.com" {
				return nil
			} else {
				return errors.New(fmt.Sprintf("Did not receive the correct SAN info, Tag: %d, SAN: %s", tag, san))
			}
		}
	}
	return errors.New("No SAN Extension found with platform-tls-client.slack.com")
}

func receiveMessages(rtm *slack.RTM, cli *client.Client) {

	for msg := range rtm.IncomingEvents {
		fmt.Print("Event Received: ")
		switch ev := msg.Data.(type) {
		case *slack.HelloEvent:
			// Ignore hello

		case *slack.ConnectedEvent:
			fmt.Println("Infos:", ev.Info)
			fmt.Println("Connection counter:", ev.ConnectionCount)
			// Replace C2147483705 with your Channel ID
			//rtm.SendMessage(rtm.NewOutgoingMessage("Hello world", "C2147483705"))

		case *slack.MessageEvent:
			fmt.Printf("Message: %v\n", ev)
			// Publish a message.
			strMsg, err := json.Marshal(convertToChatlibMessage(&ev.Msg))
			if err != nil {
				log.Println("Failed to marshall message:", err)
				continue
			}
			err = cli.Publish(&client.PublishOptions{
				QoS:       mqtt.QoS0,
				TopicName: []byte("slack/incoming"),
				Message:   []byte(strMsg),
			})
			if err != nil {
				log.Println(err)
			}

		case *slack.PresenceChangeEvent:
			fmt.Printf("Presence Change: %v\n", ev)

		case *slack.LatencyReport:
			fmt.Printf("Current latency: %v\n", ev.Value)

		case *slack.RTMError:
			fmt.Printf("Error: %s\n", ev.Error())

		case *slack.InvalidAuthEvent:
			fmt.Printf("Invalid credentials")
			return

		default:

			// Ignore other events..
			// fmt.Printf("Unexpected: %v\n", msg.Data)
		}
	}

}

func convertToChatlibMessage(incoming *slack.Msg) *chatlib.Message {
	return &chatlib.Message{
		Type:    incoming.Type,
		Channel: incoming.Channel,
		Text:    incoming.Text,
		User:    incoming.User,
	}
}

func convertSlackMessage(rtm *slack.RTM, incoming *chatlib.Message) *slack.OutgoingMessage {
	return rtm.NewOutgoingMessage(incoming.Text, incoming.Channel)
}
