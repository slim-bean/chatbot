package main

import (
	"encoding/json"
	"github.com/yosssi/gmq/mqtt"
	"github.com/yosssi/gmq/mqtt/client"
	"log"
	"oqqer.com/chatbot/pkg/chatlib"
	"os"
	"os/signal"
)

func main() {
	// Set up channel on which to send signal notifications.
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, os.Interrupt, os.Kill)

	//Create the client
	cli, err := chatlib.Connect("fu")
	if err != nil {
		panic(err)
	}

	log.Println("Starting")

	//Create a message handler
	handler := func(topicName, message []byte) {
		var m chatlib.Message
		err := json.Unmarshal(message, &m)
		if err != nil {
			log.Println(err)
		}
		log.Printf("Message: %+v\n", m)
		err = cli.Publish(&client.PublishOptions{
			QoS:       mqtt.QoS0,
			TopicName: []byte("slack/outgoing"),
			Message:   message,
		})
		if err != nil {
			log.Println(err)
		}

	}

	//Subscribe and listen for messages
	err = chatlib.Subscribe(cli, "slack/incoming", handler)
	log.Println("FU Connected and running")
	if err != nil {
		panic(err)
	}

	// Wait for a signal (shutdown/interrupt)
	<-sigc
	log.Println("Shutting Down FU")
	cli.Disconnect()
}
