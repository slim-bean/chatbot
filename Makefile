
slack:
	go build -o ./cmd/slack/slack ./cmd/slack/main.go

twitter:
	go build -o ./cmd/twitter/twitter ./cmd/twitter/main.go



slack-image:
	docker buildx build --platform linux/amd64,linux/arm/v7 -f cmd/slack/Dockerfile --push -t slimbean/chatbot-slack .

twitter-image:
	docker buildx build --platform linux/amd64,linux/arm/v7 -f cmd/twitter/Dockerfile --push -t slimbean/chatbot-twitter .

images: slack-image twitter-image

clean:
	rm -rf cmd/slack/slack
	rm -rf cmd/twitter/twitter