

## Running from GoLand

install mosquitto broker

on net vm run `sudo /rw/config/forward_port_3000`

on develop vm run `sudo iptables -I INPUT -p tcp --dport 3000 -j ACCEPT`

## SSL

https://api.slack.com/docs/verifying-requests-from-slack#mutual_tls

I download the DER digicert cert they provided: https://dl.cacerts.digicert.com/DigiCertSHA2SecureServerCA.crt

Then with openssl converted it to PEM:

```bash
openssl x509 -in DigiCertSHA2SecureServerCA.crt -inform DER -out slack_int_ca.pem -outform PEM
openssl x509 -in slack_int_ca.pem -noout -text
```

The second command is really just to verify

I used this cert in my nginx proxy to setup TLS Mutual Auth


`env GOOS=linux GOARCH=arm GOARM=7 go install`

`scp ../../../../bin/linux_arm/slack pi@172.20.82.20:`
`scp ../../../../bin/linux_arm/twitter pi@172.20.82.20:`

## Build Machine

Not totally sure if this is necessary:

Create/edit `/etc/docker/deamon.json`:

```json
{
	"experimental": true
}
```

This was necessary:

Edit `~/.docker/config.json` to make sure it includes

```json
"experimental": "enabled"
```
```
# this is the latest hash for the project
docker run --rm --privileged docker/binfmt:66f9012c56a8316f9244ffd7622d7c21c1f6f28d
docker buildx create --name raspi
docker buildx use raspi
docker buildx inspect --bootstrap
```

After reboot:
```
docker run --rm --privileged docker/binfmt:66f9012c56a8316f9244ffd7622d7c21c1f6f28d
docker restart buildx_buildkit_raspi0
```


## Target Machine
On the raspberry pi:

```
sudo apt install python-pip libffi-dev libssl-dev
sudo pip install docker-compose
```

Updating an image

push image
docker-compose pull [service]
docker-compose up -d --force-recreate [service]

mkdir tmp
docker buildx build --platform linux/amd64,linux/arm/v7 -f cmd/docker-driver/Dockerfile --build-arg BUILD_IMAGE=golang:1.13-buster --output=type=local,dest=./tmp .
mkdir cmd/docker-driver/rootfs
cp -r tmp/linux_arm_v7/* cmd/docker-driver/rootfs/
docker plugin create slimbean/loki-docker-driver cmd/docker-driver/
docker plugin push slimbean/loki-docker-driver:latest